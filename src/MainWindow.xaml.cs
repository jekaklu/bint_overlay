﻿using System.Windows;
using System.Windows.Navigation;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json.Linq;
using BINT.Models;
using BINT.Overlays;
using BINT.Static;
using BINT.Utils;
using System;
using System.Globalization;
using System.Windows.Controls;
using System.Linq;

namespace BINT {

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged, JSONParserResponseListener {

        private static MainWindow instance;
        public static MainWindow getInstance() {
            return instance;
        }

        private const string URL_API_CHEATS = "https://api.battleinterfaces.com/cheats";

        public static readonly DamagePanelWindow mDamagePanelWindow = new DamagePanelWindow();
        public static readonly ReceivedDamageAnimation mReceivedDamageAnimation = new ReceivedDamageAnimation();

        public static readonly Dictionary<string, Cheat> CHEATS = new Dictionary<string, Cheat>();

        public static Size DISPLAY_SIZE = Common.GetVirtualDisplaySize();

        public MainWindow() {
            instance = this;
            DataContext = this;
            Debug.WriteLine("Display: w=" + DISPLAY_SIZE.Width + ", h=" + DISPLAY_SIZE.Height);
            InitializeComponent();

            CultureInfo currLang = App.Language;
            RBLangEn.Tag = App.Languages.ElementAt(0);
            RBLangEn.IsChecked = App.Languages.ElementAt(0).Equals(currLang);
            RBLangEn.Checked += ChangeLanguageClick;
            RBLangRu.Tag = App.Languages.ElementAt(1);
            RBLangRu.IsChecked = App.Languages.ElementAt(1).Equals(currLang);
            RBLangRu.Checked += ChangeLanguageClick;

            ChangeLanguageClick(new object(), new EventArgs());
        }

        private void ChangeLanguageClick(Object sender, EventArgs e)
        {            
            RadioButton rb = sender as RadioButton;
            if (rb != null)
            {
                CultureInfo lang = rb.Tag as CultureInfo;
                if (lang != null)
                {
                    App.Language = lang;
                }
            }
            VisibilityToggleOfCauseDamageState = VisibilityToggleOfCauseDamageState.Equals("Show") || VisibilityToggleOfCauseDamageState.Equals("Показать") ? (string)Application.Current.Resources["m_PropertyShow"] : (string)Application.Current.Resources["m_PropertyHide"];
            VisibilityToggleOfBlockedDamageState = VisibilityToggleOfBlockedDamageState.Equals("Show") || VisibilityToggleOfBlockedDamageState.Equals("Показать") ? (string)Application.Current.Resources["m_PropertyShow"] : (string)Application.Current.Resources["m_PropertyHide"];
            VisibilityToggleOfReceivedDamageState = VisibilityToggleOfReceivedDamageState.Equals("Show") || VisibilityToggleOfReceivedDamageState.Equals("Показать") ? (string)Application.Current.Resources["m_PropertyShow"] : (string)Application.Current.Resources["m_PropertyHide"];
            DamagePanelToggleState = DamagePanelToggleState.Equals("ENABLE") || DamagePanelToggleState.Equals("ВКЛЮЧИТЬ") ?  ((string)Application.Current.Resources["m_ActivateBtnEnable"]).ToUpper() : ((string)Application.Current.Resources["m_ActivateBtnDisable"]).ToUpper();
        }

        public static string
            PROPERTY_STATE_DAMAGE_PANEL_TOGGLE = "DamagePanelToggleState",
            PROPERTY_STATE_VISIBILITY_TOGGLE_OF_CAUSE_DAMAGE = "VisibilityToggleOfCauseDamageState",
            PROPERTY_STATE_VISIBILITY_TOGGLE_OF_BLOCKED_DAMAGE = "VisibilityToggleOfBlockedDamageState",
            PROPERTY_STATE_VISIBILITY_TOGGLE_OF_RECEIVED_DAMAGE = "VisibilityToggleOfReceivedDamageState";
            //PROPERTY_STATE_ENABLE = (string)Application.Current.Resources["m_ActivateBtnEnable"],
            //PROPERTY_STATE_DISABLE = (string)Application.Current.Resources["m_ActivateBtnDisable"],
            //PROPERTY_STATE_SHOW = (string)Application.Current.Resources["m_PropertyShow"],
            //PROPERTY_STATE_HIDE = (string)Application.Current.Resources["m_PropertyHide"];

        public static bool IS_COMPLITE_LOADED_CHEATS;
        public void JSONParserResponseData(string responseData) {
            //Debug.WriteLine("responseData: " + responseData);

            JObject json = JObject.Parse(responseData);

            string version = (string) json[Constants.JSON_KEY_VERSION],
                   compatibility = (string) json[Constants.JSON_KEY_COMPATIBILITY];

            JArray data = (JArray) json[Constants.JSON_KEY_DATA];

            //List<Cheat> CheatsList = JsonConvert.DeserializeObject<List<Cheat>>(data.ToString());

            foreach (JObject o in data) {
                CHEATS.Add((string) o[Constants.JSON_KEY_NAME], new Cheat((string) o[Constants.JSON_KEY_BASE_ADDRESS], (JArray) o[Constants.JSON_KEY_OFFSETS]));
            }

            foreach (KeyValuePair<string, Cheat> entry in CHEATS) {
                Debug.WriteLine("Key: " + entry.Key + ", Value: baseAddress=" + entry.Value.baseAddress + "; offsets=" + string.Join(",", entry.Value.offsets));
            }

            IS_COMPLITE_LOADED_CHEATS = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string _DamagePanelToggleState = ((string)Application.Current.Resources["m_ActivateBtnEnable"]).ToUpper();
        public string DamagePanelToggleState {
            get { return _DamagePanelToggleState; }
            set {
                _DamagePanelToggleState = value;
                NotifyPropertyChanged(PROPERTY_STATE_DAMAGE_PANEL_TOGGLE);
            }
        }

        private bool isDisabled;
        private void DamagePanel_Toggle(object sender, RoutedEventArgs e) {
            isDisabled = !isDisabled;
            if (isDisabled) {
                mDamagePanelWindow.startMemoryOperator();
                mReceivedDamageAnimation.showDamageAnimation();
            } else {
                mDamagePanelWindow.stopMemoryOperator();
                mReceivedDamageAnimation.hideDamageAnimation();
            }
        }

        private string _VisibilityToggleOfCauseDamageState = (string)Application.Current.Resources["m_PropertyHide"];
        public string VisibilityToggleOfCauseDamageState {
            get { return _VisibilityToggleOfCauseDamageState; }
            set {
                _VisibilityToggleOfCauseDamageState = value;
                NotifyPropertyChanged(PROPERTY_STATE_VISIBILITY_TOGGLE_OF_CAUSE_DAMAGE);
            }
        }
        private void visibilityToggleOfCauseDamage(object sender, RoutedEventArgs e) {
            Visibility visibility = mDamagePanelWindow.CauseDamageBox.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
            mDamagePanelWindow.CauseDamageBox.Visibility = visibility;
            VisibilityToggleOfCauseDamageState = visibility == Visibility.Collapsed ? (string)Application.Current.Resources["m_PropertyShow"] : (string)Application.Current.Resources["m_PropertyHide"];
        }

        private string _VisibilityToggleOfBlockedDamageState = (string)Application.Current.Resources["m_PropertyHide"];
        public string VisibilityToggleOfBlockedDamageState {
            get { return _VisibilityToggleOfBlockedDamageState; }
            set {
                _VisibilityToggleOfBlockedDamageState = value;
                NotifyPropertyChanged(PROPERTY_STATE_VISIBILITY_TOGGLE_OF_BLOCKED_DAMAGE);
            }
        }
        private void visibilityToggleOfBlockedDamage(object sender, RoutedEventArgs e) {
            Visibility visibility = mDamagePanelWindow.BlockedDamageBox.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
            mDamagePanelWindow.BlockedDamageBox.Visibility = visibility;
            VisibilityToggleOfBlockedDamageState = visibility == Visibility.Collapsed ? (string)Application.Current.Resources["m_PropertyShow"] : (string)Application.Current.Resources["m_PropertyHide"];
        }

        private string _VisibilityToggleOfReceivedDamageState = (string)Application.Current.Resources["m_PropertyHide"];
        public string VisibilityToggleOfReceivedDamageState {
            get { return _VisibilityToggleOfReceivedDamageState; }
            set {
                _VisibilityToggleOfReceivedDamageState = value;
                NotifyPropertyChanged(PROPERTY_STATE_VISIBILITY_TOGGLE_OF_RECEIVED_DAMAGE);
            }
        }

        private void visibilityToggleOfReceivedDamage(object sender, RoutedEventArgs e) {
            Visibility visibility = mDamagePanelWindow.ReceivedDamageBox.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
            mDamagePanelWindow.ReceivedDamageBox.Visibility = visibility;
            VisibilityToggleOfReceivedDamageState = visibility == Visibility.Collapsed ? (string)Application.Current.Resources["m_PropertyShow"] : (string)Application.Current.Resources["m_PropertyHide"];
        }

        private void startWoTB(object sender, RoutedEventArgs e) {
            startWoTB();
        }

        public void startWoTB() {
            Process.Start("steam://rungameid/444200");
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e) {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        public void loadCheats() {
            new JSONParser(URL_API_CHEATS, this);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            loadCheats();
        }

        private void Window_Closing(object sender, CancelEventArgs e) {
            Application.Current.Shutdown();
        }
        
    }

    /*
    public class PropertyChanged : DependencyObject {
        private readonly DependencyProperty property;
        public PropertyChanged(string key) {
            property = DependencyProperty.RegisterAttached(key, typeof(object), typeof(PropertyChanged));
        }
        public void set(DependencyObject dependency, object value) {
            dependency.SetValue(property, value);
        }
        public object get(DependencyObject dependency) {
            return dependency.GetValue(property);
        }
    }
    */

}
