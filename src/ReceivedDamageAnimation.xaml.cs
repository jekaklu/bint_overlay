﻿using BINT.MemoryUlils;
using BINT.Models;
using BINT.Static;
using BINT.Windows;
using BINT.WindowsInterfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BINT
{
    /// <summary>
    /// Логика взаимодействия для ReceivedDamageAnimation.xaml
    /// </summary>
    public partial class ReceivedDamageAnimation : Window
    {
        public ReceivedDamageAnimation()
        {
            InitializeComponent();
        }

        private IntPtr
            HWND_WOTB,
            HWND_DamageAnimationWindow;

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            HWND_DamageAnimationWindow = new WindowInteropHelper(this).Handle;
            WindowsServices.SetWindowExTransparent(HWND_DamageAnimationWindow);
            if (HWND_DamageAnimationWindow != IntPtr.Zero)
            {
                //SetWindowPos(hWndOverlay, IntPtr.Zero, 0, (int) displaySize.Height / 2 - (int) this.Height / 2, 0, 0, User32.SWP_NOSIZE | User32.SWP_NOZORDER | User32.SWP_SHOWWINDOW | User32.SWP_NOACTIVATE);
            }
        }

        private Thread ActualPositionOfWoTBWindowThread;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ActualPositionOfWoTBWindowThread = new Thread(ActualPositionOfWoTBWindow);
            ActualPositionOfWoTBWindowThread.IsBackground = true;
            ActualPositionOfWoTBWindowThread.Start();
        }

        private void ActualPositionOfWoTBWindow()
        {
            while (true)
            {
                HWND_WOTB = User32.FindWindow(null, Constants.WINDOW_NAME_WOTB);
                if (HWND_WOTB != null)
                {
                    RECT rect = new RECT();
                    User32.GetWindowRect(HWND_WOTB, out rect);
                    try
                    {
                        Dispatcher.Invoke(new Action(() => {
                            int width = rect.right - rect.left,
                                height = rect.bottom - rect.top;
                            User32.SetWindowPos(HWND_DamageAnimationWindow, User32.HWND_TOPMOST/*IntPtr.Zero*/, rect.left + ((int)(width / 3) - (int)Height / 2), rect.top + ((int)(height / 1.7) - (int)Height / 2), 0, 0, User32.SWP_NOSIZE | User32.SWP_NOZORDER | User32.SWP_SHOWWINDOW | User32.SWP_NOACTIVATE);
                        }));
                    }
                    catch (TaskCanceledException) { }
                }
                Thread.Sleep(50);
            }
        }

        public void hideDamageAnimation() {
            Dispatcher.Invoke(new Action(() => {
                Hide();
            }));
        }

        public void showDamageAnimation() {
            Dispatcher.Invoke(new Action(() => {                
                Show();
            }));
        }
    }
}
