﻿
namespace BINT.Static {
    public class Constants {
        public static readonly string
            URL_API_CHEATS = "https://api.battleinterfaces.com/cheats",
            PROCESS_NAME_WOTB = "wotblitz",
            WINDOW_NAME_WOTB = "WoT Blitz",
            JSON_KEY_VERSION = "version",
            JSON_KEY_COMPATIBILITY = "compatibility",
            JSON_KEY_DATA = "data",
            JSON_KEY_NAME = "name",
            JSON_KEY_BASE_ADDRESS = "base_address",
            JSON_KEY_OFFSETS = "offsets",
            CHEAT_KEY_CAUSE_DAMAGE = "cause_damage",
            CHEAT_KEY_BLOCKED_DAMAGE = "blocked_damage",
            CHEAT_KEY_HIT_POINTS = "hit_points",
            CHEAT_KEY_IS_HANGAR = "is_hangar",
            CHEAT_KEY_NUMBER_OF_SELECTED_TANK = "selected_tank_number",
            CHEAT_KEY_QUANTITY_TANKS_IN_HANGAR = "quantity_tanks_in_hangar";
    }
}
