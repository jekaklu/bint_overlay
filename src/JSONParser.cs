﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BINT
{
    public interface JSONParserResponseListener {
        void JSONParserResponseData(string responseData);
    }
    
    public class JSONParser {
        
        private readonly JSONParserResponseListener listener;
        public JSONParser(string url, JSONParserResponseListener listener) {
            this.listener = listener;
            get(url);
        }

        async private void get(string url) {
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri(url);
                //client.DefaultRequestHeaders.Add("User-Agent", "Anything");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                Task<HttpResponseMessage> getResponse = client.GetAsync(url);
                HttpResponseMessage response = await getResponse;
                var resultString = await response.Content.ReadAsStringAsync();

                //var response = client.GetAsync("").Result;
                //response.EnsureSuccessStatusCode();
                //string result = response.Content.ReadAsStringAsync().Result;
                //JObject obj = JsonConvert.DeserializeObject(result);

                if (resultString != null) listener.JSONParserResponseData(resultString);
            }
        }
    }
}
