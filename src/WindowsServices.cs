﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using BINT.WindowsInterfaces;

namespace BINT.Windows
{
    public class WindowsServices  {

        public static void SetWindowExTransparent(IntPtr hwnd) {
            var extendedStyle = User32.GetWindowLong(hwnd, User32.GWL_EXSTYLE);
            User32.SetWindowLong(hwnd, User32.GWL_EXSTYLE, extendedStyle | User32.WS_EX_TRANSPARENT);
        }

        public static Process GetParentProcess(int pid) {
            Process parentProc = null;
            IntPtr handleToSnapshot = IntPtr.Zero;
            try {
                Kernel32.PROCESSENTRY32 procEntry = new Kernel32.PROCESSENTRY32();
                procEntry.dwSize = (UInt32)Marshal.SizeOf(typeof(Kernel32.PROCESSENTRY32));
                handleToSnapshot = Kernel32.CreateToolhelp32Snapshot((uint)Kernel32.SnapshotFlags.Process, 0);
                if (Kernel32.Process32First(handleToSnapshot, ref procEntry)) {
                    do {
                        if (pid == procEntry.th32ProcessID) {
                            parentProc = Process.GetProcessById((int)procEntry.th32ParentProcessID);
                            break;
                        }
                    } while (Kernel32.Process32Next(handleToSnapshot, ref procEntry));
                } else throw new ApplicationException(string.Format("Failed with win32 error code {0}", Marshal.GetLastWin32Error()));
            } catch (Exception ex) {
                throw new ApplicationException("Can't get the process.", ex);
            } finally {
                Kernel32.CloseHandle(handleToSnapshot);
            }
            return parentProc;
        }
    }
}
