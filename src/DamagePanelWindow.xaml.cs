﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;

using BINT.MemoryUlils;
using BINT.Models;
using BINT.Static;
using BINT.WindowsInterfaces;
using BINT.Windows;
using System.Diagnostics;

namespace BINT.Overlays
{
    public partial class DamagePanelWindow : Window, MemoryOperator.Listener
    {
        private const string
            COLOR_RED = "#DA7070",
            COLOR_GENDER = "#E19154",
            COLOR_YELLOW = "#D5C363",
            COLOR_GREEN = "#8ADA4C",
            COLOR_TURQUOISE = "#7BCCCA",
            COLOR_PURPLE = "#B5A0E5";

        private int
            RECEIVED_DAMAGE_COUNTER,
            HIT_POINTS,
            HIT_POINTS_FULL,
            HIT_POINTS_PREVIOS,
            CURRENT_CAUSE_DAMAGE = -1,
            CURRENT_BLOCKED_DAMAGE = -1,
            CURRENT_HIT_POINTS = -1;

        private bool 
            CURRENT_IS_HANGAR;

        private IntPtr
            HWND_WOTB,
            HWND_DamagePanelWindow;

        private MemoryOperator mMemoryOperator;
        public void startMemoryOperator() {            
            mMemoryOperator = new MemoryOperator(this);
        }

        public void stopMemoryOperator() {
            Dispatcher.Invoke(new Action(() => {
                if (mMemoryOperator != null) {
                    mMemoryOperator.stop();
                    MainWindow.getInstance().DamagePanelToggleState = ((string)Application.Current.Resources["m_ActivateBtnEnable"]).ToUpper();
                    Hide();
                }
            }));
        }

        public DamagePanelWindow() {
            InitializeComponent();
        }

        private Thread ActualPositionOfWoTBWindowThread;
        private void Window_Loaded(object sender, RoutedEventArgs e) {
            ActualPositionOfWoTBWindowThread = new Thread(ActualPositionOfWoTBWindow);
            ActualPositionOfWoTBWindowThread.IsBackground = true;
            ActualPositionOfWoTBWindowThread.Start();
        }
        
        protected override void OnSourceInitialized(EventArgs e) {
            base.OnSourceInitialized(e);
            HWND_DamagePanelWindow = new WindowInteropHelper(this).Handle;
            WindowsServices.SetWindowExTransparent(HWND_DamagePanelWindow);
            if (HWND_DamagePanelWindow != IntPtr.Zero) {
                //SetWindowPos(hWndOverlay, IntPtr.Zero, 0, (int) displaySize.Height / 2 - (int) this.Height / 2, 0, 0, User32.SWP_NOSIZE | User32.SWP_NOZORDER | User32.SWP_SHOWWINDOW | User32.SWP_NOACTIVATE);
            }
        }

        public void successfulStartup() {
            // Если MemoryOperator успешно стартовал, то показываем оверлей
            Dispatcher.Invoke(new Action(() => {
                MainWindow.getInstance().DamagePanelToggleState = ((string)Application.Current.Resources["m_ActivateBtnDisable"]).ToUpper();
                //MainWindow.getInstance().inBattleButton.Visibility = Visibility.Collapsed;
                Show();
            }));
        }
        
        public void isHangar(bool isHangar) {
            if (CURRENT_IS_HANGAR != isHangar) {
                Dispatcher.Invoke(new Action(() => {
                    if (isHangar) {
                        CURRENT_CAUSE_DAMAGE = CURRENT_BLOCKED_DAMAGE = CURRENT_HIT_POINTS = -1;
                        RECEIVED_DAMAGE_COUNTER = HIT_POINTS_FULL = HIT_POINTS_PREVIOS = 0;
                        ReceivedDamageList.Text = "---";
                    }
                    Visibility = isHangar ? Visibility.Hidden : Visibility.Visible;
                    //ReceivedDamageAnimation.getInstance().WindowAnimation.Visibility = isHangar ? Visibility.Hidden : Visibility.Visible;
                    foreach (Window window in Application.Current.Windows)
                    {
                        if (window.GetType() == typeof(ReceivedDamageAnimation))
                        {
                            (window as ReceivedDamageAnimation).Visibility = isHangar ? Visibility.Hidden: Visibility.Visible;
                        }
                    }
                    CURRENT_IS_HANGAR = isHangar;
                }));
            }
        }

        public void causeDamage(int causeDamage) {
            if (CURRENT_IS_HANGAR) return;
            if (CURRENT_CAUSE_DAMAGE != causeDamage) printCauseDamage(CURRENT_CAUSE_DAMAGE = causeDamage);
        }

        public void blockedDamage(int blockedDamage) {
            if (CURRENT_IS_HANGAR) return;
            if (CURRENT_BLOCKED_DAMAGE != blockedDamage) printBlockedDamage(CURRENT_BLOCKED_DAMAGE = blockedDamage);
        }

        public void hitPoints(int hitPoints) {
            if (CURRENT_IS_HANGAR) return;

            if (hitPoints > 0 && HIT_POINTS_FULL == 0) HIT_POINTS_FULL = hitPoints;

            if (CURRENT_HIT_POINTS == -1 && hitPoints < HIT_POINTS_FULL || CURRENT_HIT_POINTS > hitPoints && hitPoints < 5000) {
                printReceivedDamage(CURRENT_HIT_POINTS = hitPoints);
            }

            HIT_POINTS = hitPoints;
        }

        public void processNotFound() {
            Dispatcher.Invoke(new Action(() => {
                MainWindow.getInstance().DamagePanelToggleState = ((string)Application.Current.Resources["m_ActivateBtnEnable"]).ToUpper();
                MainWindow.getInstance().inBattleButton.Visibility = Visibility.Visible;
                Hide();
                // Дальше диалог чтобы юзер запустил игру
                MessageBoxResult res = MessageBox.Show((string)Application.Current.Resources["m_WarningNoProcessGame"], "", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (res == MessageBoxResult.OK) {
                    MainWindow.getInstance().startWoTB();
                } else
                if (res == MessageBoxResult.Cancel) {
                    //
                }
            }));
        }

        public void getCheatsError() {
            Dispatcher.Invoke(new Action(() => {
                MainWindow.getInstance().DamagePanelToggleState = ((string)Application.Current.Resources["m_ActivateBtnEnable"]).ToUpper();
                Hide();
                // Дальше диалог об ошибке и просьбе перезапустить приложение или диалог 
                // где юзер нажмет "повторить" и снова запуститься метод:
                // MainWindow.getInstance().loadCheats();
                MessageBoxResult res = MessageBox.Show((string)Application.Current.Resources["m_WarningNoData"], "", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (res == MessageBoxResult.OK) {
                    MainWindow.getInstance().loadCheats();
                } else
                if (res == MessageBoxResult.Cancel) {
                    Application.Current.Shutdown();
                }
            }));
        }

        private void ActualPositionOfWoTBWindow() {
            while (true) {
                HWND_WOTB = User32.FindWindow(null, Constants.WINDOW_NAME_WOTB);
                if (HWND_WOTB != null) {
                    RECT rect = new RECT();
                    User32.GetWindowRect(HWND_WOTB, out rect);
                    try {
                        Dispatcher.Invoke(new Action(() => {
                            int width = rect.right - rect.left,
                                height = rect.bottom - rect.top;
                            //Debug.WriteLine("this.Top=" + this.Top + ", this.Left=" + this.Left);
                            User32.SetWindowPos(HWND_DamagePanelWindow, User32.HWND_TOPMOST/*IntPtr.Zero*/, rect.left + (width == MainWindow.DISPLAY_SIZE.Width && height == MainWindow.DISPLAY_SIZE.Height ? -1 : 7), rect.top + (height / 2 - (int) Height / 2), 0, 0, User32.SWP_NOSIZE | User32.SWP_NOZORDER | User32.SWP_SHOWWINDOW | User32.SWP_NOACTIVATE);
                        }));
                    } catch (TaskCanceledException) {}
                }
                Thread.Sleep(50);
            }
        }

        private void printCauseDamage(int value) {
            if (value < 0 || value > 20000) return;
            try {
                LabelCauseDamage.Dispatcher.Invoke(new Action(() => {
                    LabelCauseDamage.Foreground = new SolidColorBrush(parseHexColor(getColorByValue(value, 500, 750, 1250, 2000, 3000)));
                    LabelCauseDamage.Content = value.ToString();
                }));
            } catch (TaskCanceledException) {}
        }
        
        private void printBlockedDamage(int value) {
            if (value < 0 || value > 20000) return;
            try {
                LabelBlockedDamage.Dispatcher.Invoke(new Action(() => {
                    LabelBlockedDamage.Content = value.ToString();
                }));
            } catch (TaskCanceledException) {}
            
        }

        private static string DelLastStr(string s) {
            int count = s.Split('\n').Length;
            if (count > 10) return s.Substring(0, s.LastIndexOf('\n'));
            else return s;
        }

        private void printReceivedDamage(int hitPoints) {
            ReceivedDamageList.Dispatcher.Invoke(new Action(() => {
                if (string.Equals(ReceivedDamageList.Text, "---")) ReceivedDamageList.Text = "";
                RECEIVED_DAMAGE_COUNTER++;
                ReceivedDamageList.Text = RECEIVED_DAMAGE_COUNTER + " - " + (HIT_POINTS_PREVIOS == 0 ? HIT_POINTS_FULL - hitPoints : HIT_POINTS_PREVIOS - hitPoints) + (RECEIVED_DAMAGE_COUNTER == 1 ? "" : "\n") + ReceivedDamageList.Text.ToString();
                foreach (Window window in Application.Current.Windows)
                {
                    if (window.GetType() == typeof(ReceivedDamageAnimation))
                    {
                        if ((HIT_POINTS_PREVIOS == 0 ? HIT_POINTS_FULL - hitPoints : HIT_POINTS_PREVIOS - hitPoints) < 3000)
                        {
                            (window as ReceivedDamageAnimation).animateTB.Content = (HIT_POINTS_PREVIOS == 0 ? HIT_POINTS_FULL - hitPoints : HIT_POINTS_PREVIOS - hitPoints);
                            (window as ReceivedDamageAnimation).animateTB.Opacity = 0;
                            if ((window as ReceivedDamageAnimation).animateTB.Height != 60)
                                (window as ReceivedDamageAnimation).animateTB.Height = 60;
                            else
                                (window as ReceivedDamageAnimation).animateTB.Height = 59;
                        }
                    }
                }
                HIT_POINTS_PREVIOS = hitPoints;
                ReceivedDamageList.Text = DelLastStr(ReceivedDamageList.Text.ToString());
            }));
        }

        private static String getColorByValue(int value, double red, double ginger, double yellow, double green, double turquoise) {
            if (value < red) return COLOR_RED; else
            if (value < ginger) return COLOR_GENDER; else
            if (value < yellow) return COLOR_YELLOW; else
            if (value < green) return COLOR_GREEN; else
            if (value < turquoise) return COLOR_TURQUOISE; else
                return COLOR_PURPLE;
        }

        private Color parseHexColor(string color)  {
            return (Color) System.Windows.Media.ColorConverter.ConvertFromString(color);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            System.Windows.Application.Current.Shutdown();
        }

    }

}
