﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using BINT.Models;
using BINT.Static;
using BINT.WindowsInterfaces;

namespace BINT.MemoryUlils {
    public class MemoryOperator
    {

        private static bool
            IS_RUNNING,
            CHEATS_GOT,
            SUCCESSFUL_STARTUP,
            IS_CB_RANDOM_TANK_IS_CHECKED,
            LVL1_IS_CHECKED,
            LVL2_IS_CHECKED,
            LVL3_IS_CHECKED,
            LVL4_IS_CHECKED,
            LVL5_IS_CHECKED,
            LVL6_IS_CHECKED,
            LVL7_IS_CHECKED,
            LVL8_IS_CHECKED,
            LVL9_IS_CHECKED,
            LVL10_IS_CHECKED;

        private readonly int
            PROCESS_WM_READ = 0x0010,
            PROCESS_VM_WRITE = 0x0020,
            PROCESS_VM_OPERATION = 0x0008,
            PROCESS_QUERY_INFORMATION = 0x0400;
        
        private IntPtr hProcess;

        private Thread ProcessInBattleThread;
        private Thread ProcessInHangarThread;

        private Listener listener;
        public interface Listener {
            void successfulStartup();
            void isHangar(bool isHangar);
            void causeDamage(int causeDamage);
            void blockedDamage(int blockedDamage);
            void hitPoints(int hitPoints);
            void processNotFound();
            void getCheatsError();
        }

        public MemoryOperator(Listener listener) {
            if (!IS_RUNNING) {
                IS_RUNNING = true;
                this.listener = listener;
                ProcessInBattleThread = new Thread(processInBattle);
                ProcessInBattleThread.IsBackground = true;
                ProcessInBattleThread.Start();
                ProcessInHangarThread = new Thread(processInHangar);
                ProcessInHangarThread.IsBackground = true;
                ProcessInHangarThread.Start();
            }
        }

        private void processInBattle() {

            Cheat causeDamage = null,
                  blockedDamage = null,
                  hitPoints = null,
                  isHangar = null;/*,
                  numberOfSelectedTank = null,
                  quantityTanksInHangar = null;
            Random rndSelectTank = new Random();*/
            while (IS_RUNNING) {

                Process process = Process.GetProcessesByName(Constants.PROCESS_NAME_WOTB).FirstOrDefault();
                /*Application.Current.Dispatcher.Invoke(new System.Action(() => {
                    IS_CB_RANDOM_TANK_IS_CHECKED = MainWindow.getInstance().checkBoxRandomTank.IsChecked.Value;
                    LVL1_IS_CHECKED = MainWindow.getInstance().Lvl1.IsChecked.Value;
                    LVL2_IS_CHECKED = MainWindow.getInstance().Lvl2.IsChecked.Value;
                    LVL3_IS_CHECKED = MainWindow.getInstance().Lvl3.IsChecked.Value;
                    LVL4_IS_CHECKED = MainWindow.getInstance().Lvl4.IsChecked.Value;
                    LVL5_IS_CHECKED = MainWindow.getInstance().Lvl5.IsChecked.Value;
                    LVL6_IS_CHECKED = MainWindow.getInstance().Lvl6.IsChecked.Value;
                    LVL7_IS_CHECKED = MainWindow.getInstance().Lvl7.IsChecked.Value;
                    LVL8_IS_CHECKED = MainWindow.getInstance().Lvl8.IsChecked.Value;
                    LVL9_IS_CHECKED = MainWindow.getInstance().Lvl9.IsChecked.Value;
                    LVL10_IS_CHECKED = MainWindow.getInstance().Lvl10.IsChecked.Value;
                }));*/
                

                if (process != null) {

                    hProcess = Kernel32.OpenProcess(PROCESS_WM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION | PROCESS_QUERY_INFORMATION, false, process.Id);

                    if (IS_RUNNING)
                    if (!CHEATS_GOT) {

                        if (MainWindow.IS_COMPLITE_LOADED_CHEATS) {
                            causeDamage = MainWindow.CHEATS[Constants.CHEAT_KEY_CAUSE_DAMAGE];
                            blockedDamage = MainWindow.CHEATS[Constants.CHEAT_KEY_BLOCKED_DAMAGE];
                            hitPoints = MainWindow.CHEATS[Constants.CHEAT_KEY_HIT_POINTS];
                            isHangar = MainWindow.CHEATS[Constants.CHEAT_KEY_IS_HANGAR];
                            /*numberOfSelectedTank = MainWindow.CHEATS[Constants.CHEAT_KEY_NUMBER_OF_SELECTED_TANK];
                            quantityTanksInHangar = MainWindow.CHEATS[Constants.CHEAT_KEY_QUANTITY_TANKS_IN_HANGAR];*/
                            CHEATS_GOT = true;
                        } else Debug.WriteLine("Cheats not yet loaded!");

                    } else
                    if (causeDamage != null &&
                        blockedDamage != null &&
                        hitPoints != null &&
                        isHangar != null/* && 
                        numberOfSelectedTank != null &&
                        quantityTanksInHangar != null*/) {

                        if (!SUCCESSFUL_STARTUP) {
                            listener.successfulStartup();
                            SUCCESSFUL_STARTUP = true;
                        }

                            IntPtr BaseAddress = process.MainModule.BaseAddress,
                                   CauseDamageAddress = IntPtr.Add(BaseAddress, causeDamage.baseAddress),
                                   BlockedDamageAddress = IntPtr.Add(BaseAddress, blockedDamage.baseAddress),
                                   HitPointsAddress = IntPtr.Add(BaseAddress, hitPoints.baseAddress),
                                   IsHangarAddress = IntPtr.Add(BaseAddress, isHangar.baseAddress);
                               /*NumberOfSelectedTankAddress = IntPtr.Add(BaseAddress, numberOfSelectedTank.baseAddress),
                               QuantityTanksInHangarAddress = IntPtr.Add(BaseAddress, quantityTanksInHangar.baseAddress);*/

                            //int tmp = 10;
                        int IS_HANGAR = readInt(IsHangarAddress, null),
                            CAUSE_DAMAGE = readInt(CauseDamageAddress, causeDamage.offsets),
                            BLOCKED_DAMAGE = readInt(CauseDamageAddress, blockedDamage.offsets),
                            HIT_POINTS = readInt(HitPointsAddress, hitPoints.offsets);
                            /*if ((IS_CB_RANDOM_TANK_IS_CHECKED && IS_HANGAR == 1) && 
                                ((LVL1_IS_CHECKED && tmp == 1) || 
                                (LVL2_IS_CHECKED && tmp == 2) || 
                                (LVL3_IS_CHECKED && tmp == 3) ||
                                (LVL4_IS_CHECKED && tmp == 4) ||
                                (LVL5_IS_CHECKED && tmp == 5) ||
                                (LVL6_IS_CHECKED && tmp == 6) ||
                                (LVL7_IS_CHECKED && tmp == 7) ||
                                (LVL8_IS_CHECKED && tmp == 8) ||
                                (LVL9_IS_CHECKED && tmp == 9) ||
                                (LVL10_IS_CHECKED && tmp == 1))
                                )
                                WriteInt(NumberOfSelectedTankAddress, numberOfSelectedTank.offsets, rndSelectTank.Next(
                                    readInt(NumberOfSelectedTankAddress, quantityTanksInHangar.offsets)));*/

                        listener.isHangar(IS_HANGAR == 1);
                        listener.causeDamage(CAUSE_DAMAGE);
                        listener.blockedDamage(BLOCKED_DAMAGE);
                        listener.hitPoints(HIT_POINTS);

                    } else {
                        stop();
                        listener.getCheatsError();
                        Debug.WriteLine("One or more cheats == null");
                        return;
                    }
                } else {
                    stop();
                    listener.processNotFound();
                    Debug.WriteLine("Process not exists!");
                    return;
                }
                Thread.Sleep(100);
            }
        }

        private void processInHangar() {
            Cheat isHangar = null,
                  numberOfSelectedTank = null,
                  quantityTanksInHangar = null;
            Random rndSelectTank = new Random();
            while (IS_RUNNING) {
                Process process = Process.GetProcessesByName(Constants.PROCESS_NAME_WOTB).FirstOrDefault();
                Application.Current.Dispatcher.Invoke(new System.Action(() => {
                    IS_CB_RANDOM_TANK_IS_CHECKED = MainWindow.getInstance().checkBoxRandomTank.IsChecked.Value;
                    LVL1_IS_CHECKED = MainWindow.getInstance().Lvl1.IsChecked.Value;
                    LVL2_IS_CHECKED = MainWindow.getInstance().Lvl2.IsChecked.Value;
                    LVL3_IS_CHECKED = MainWindow.getInstance().Lvl3.IsChecked.Value;
                    LVL4_IS_CHECKED = MainWindow.getInstance().Lvl4.IsChecked.Value;
                    LVL5_IS_CHECKED = MainWindow.getInstance().Lvl5.IsChecked.Value;
                    LVL6_IS_CHECKED = MainWindow.getInstance().Lvl6.IsChecked.Value;
                    LVL7_IS_CHECKED = MainWindow.getInstance().Lvl7.IsChecked.Value;
                    LVL8_IS_CHECKED = MainWindow.getInstance().Lvl8.IsChecked.Value;
                    LVL9_IS_CHECKED = MainWindow.getInstance().Lvl9.IsChecked.Value;
                    LVL10_IS_CHECKED = MainWindow.getInstance().Lvl10.IsChecked.Value;
                }));                

                if (process != null) {

                    hProcess = Kernel32.OpenProcess(PROCESS_WM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION | PROCESS_QUERY_INFORMATION, false, process.Id);

                    if (IS_RUNNING)
                    //if (!CHEATS_GOT) {

                        if (MainWindow.IS_COMPLITE_LOADED_CHEATS) {
                            isHangar = MainWindow.CHEATS[Constants.CHEAT_KEY_IS_HANGAR];
                            numberOfSelectedTank = MainWindow.CHEATS[Constants.CHEAT_KEY_NUMBER_OF_SELECTED_TANK];
                            quantityTanksInHangar = MainWindow.CHEATS[Constants.CHEAT_KEY_QUANTITY_TANKS_IN_HANGAR];
                            //CHEATS_GOT = true;
                        } else Debug.WriteLine("Cheats not yet loaded!");

                    //} else
                    if (isHangar != null && 
                        numberOfSelectedTank != null &&
                        quantityTanksInHangar != null) {

                        if (!SUCCESSFUL_STARTUP) {
                            listener.successfulStartup();
                            SUCCESSFUL_STARTUP = true;
                        }

                        IntPtr BaseAddress = process.MainModule.BaseAddress,
                               IsHangarAddress = IntPtr.Add(BaseAddress, isHangar.baseAddress),
                               NumberOfSelectedTankAddress = IntPtr.Add(BaseAddress, numberOfSelectedTank.baseAddress),
                               QuantityTanksInHangarAddress = IntPtr.Add(BaseAddress, quantityTanksInHangar.baseAddress);

                            int tmp = 2;
                            int IS_HANGAR = readInt(IsHangarAddress, null);
                            if ((IS_CB_RANDOM_TANK_IS_CHECKED && IS_HANGAR == 1) && 
                                ((LVL1_IS_CHECKED && tmp == 1) || 
                                (LVL2_IS_CHECKED && tmp == 2) || 
                                (LVL3_IS_CHECKED && tmp == 3) ||
                                (LVL4_IS_CHECKED && tmp == 4) ||
                                (LVL5_IS_CHECKED && tmp == 5) ||
                                (LVL6_IS_CHECKED && tmp == 6) ||
                                (LVL7_IS_CHECKED && tmp == 7) ||
                                (LVL8_IS_CHECKED && tmp == 8) ||
                                (LVL9_IS_CHECKED && tmp == 9) ||
                                (LVL10_IS_CHECKED && tmp == 1))
                                )
                                WriteInt(NumberOfSelectedTankAddress, numberOfSelectedTank.offsets, rndSelectTank.Next(
                                    readInt(NumberOfSelectedTankAddress, quantityTanksInHangar.offsets)));

                        listener.isHangar(IS_HANGAR == 1);
                    } else {
                        stop();
                        listener.getCheatsError();
                        Debug.WriteLine("One or more cheats == null");
                        return;
                    }
                } else {
                    stop();
                    //listener.processNotFound();
                    Debug.WriteLine("Process not exists!");
                    return;
                }
                Thread.Sleep(30);
            }
        }
        
        public int readInt(IntPtr baseAddress, int[] offsets) {
            byte[] buffer = new byte[4];
            IntPtr bytesRead = IntPtr.Zero;
            Kernel32.ReadProcessMemory(hProcess, baseAddress, buffer, 4, out bytesRead);
            if (offsets != null) foreach (int offset in offsets)
                    Kernel32.ReadProcessMemory(hProcess, (IntPtr) BitConverter.ToInt32(buffer, 0) + offset, buffer, 4, out bytesRead);
            return BitConverter.ToInt32(buffer, 0);
        }

        public void WriteInt(IntPtr baseAddress, int[] offsets, int value)
        {
            bool success;
            byte[] address = new byte[4];
            byte[] buffer = BitConverter.GetBytes(value);
            IntPtr bytesRead = IntPtr.Zero;
            Kernel32.ReadProcessMemory(hProcess, baseAddress, address, 4, out bytesRead);
            if (offsets != null) foreach (int offset in offsets)
                    if (!(offset == offsets[offsets.Length - 1]))
                    Kernel32.ReadProcessMemory(hProcess, (IntPtr)BitConverter.ToInt32(address, 0) + offset, address, 4, out bytesRead);
            success = Kernel32.WriteProcessMemory(hProcess, offsets != null ? ((IntPtr)BitConverter.ToInt32(address, 0) + offsets[offsets.Length - 1]) : baseAddress, buffer, (IntPtr)4, ref bytesRead);
        }

        public void stop() {
            IS_RUNNING = SUCCESSFUL_STARTUP = CHEATS_GOT = false;
            hProcess = IntPtr.Zero;
        }
        //Test commitq
    }
}
