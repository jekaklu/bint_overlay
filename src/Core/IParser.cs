﻿using AngleSharp.Html.Dom;

namespace BINT.Core
{
    interface IParser<T> where T : class
    {
        T Parse(IHtmlDocument document);
    }
}
