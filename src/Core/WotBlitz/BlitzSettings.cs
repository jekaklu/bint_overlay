﻿namespace BINT.Core.WotBlitz
{
    class BlitzSettings : IParserSettings
    {
        public string BaseUrl { get; set; } = "https://itunes.apple.com";
        public string Prefix { get; set; }
        public int StartPoint { get; set; }
        public int EndPoint { get; set; }
    }
}
