﻿using Newtonsoft.Json.Linq;
using System;

namespace BINT.Models
{
    public struct RECT {
        public int left, top, right, bottom;
    }

    public class Cheat {
        public readonly int baseAddress;
        //public readonly JArray offsets;
        public readonly int[] tempOffsetsForNumSelectedTank = {0x158, 0x8, 0x12C, 0x3C, 0x70 };
        public readonly int[] offsets;
        public Cheat(string baseAddress, JArray offsets) {
            this.baseAddress = Convert.ToInt32(baseAddress.Substring(2), 16);
            //this.offsets = offsets;
            this.offsets = new int[offsets.Count];
            //string[] bytesArray = offsets.ToObject<string[]>();
            for (int i = 0; i < offsets.Count; i++) {
                string offset = (string)offsets[i];
                //Debug.WriteLine("offset: "  + offset);
                //int.Parse(offset.Substring(2), NumberStyles.HexNumber);
                this.offsets[i] = Convert.ToInt32(offset.Substring(2), 16);
                //Debug.WriteLine(i + " - offset: " + offsets[i]);
            }
        }
    }

}
