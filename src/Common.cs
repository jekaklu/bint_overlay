﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;

namespace BINT.Utils {
    public class Common {

        [DllImport("user32.dll")]
        public static extern int GetSystemMetrics(SystemMetric metric);
        public enum SystemMetric {
            VirtualScreenWidth = 78, // CXVIRTUALSCREEN 0x0000004E 
            VirtualScreenHeight = 79, // CYVIRTUALSCREEN 0x0000004F 
        }

        public static Size GetVirtualDisplaySize() {
            return new Size(GetSystemMetrics(SystemMetric.VirtualScreenWidth), GetSystemMetrics(SystemMetric.VirtualScreenHeight));
        }

        public static List<string> getInstalledApps() {
            List<string> apps = new List<string>();
            string registry_key = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall";
            using (RegistryKey key = Registry.LocalMachine.OpenSubKey(registry_key)) {
                foreach (string subkey_name in key.GetSubKeyNames()) {
                    using (RegistryKey subkey = key.OpenSubKey(subkey_name)) {
                        string DisplayName = subkey.GetValue("DisplayName") as string;
                        apps.Add(DisplayName);
                        Console.WriteLine(DisplayName);
                    }
                }
            }
            return apps;
        }
    }
}
